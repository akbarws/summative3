CREATE DATABASE summativedb;

use summativedb;

CREATE TABLE student (
id INT NOT NULL AUTO_INCREMENT,
NAME VARCHAR(45) NOT NULL,
SURNAME VARCHAR(20),
BIRTHDATE DATE NOT NULL,
GENDER VARCHAR(20) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE lesson (
id INT NOT NULL AUTO_INCREMENT,
NAME VARCHAR(45) NOT NULL,
LEVEL VARCHAR(10) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE score (
id INT NOT NULL AUTO_INCREMENT,
student_id INT,
lesson_id INT,
score INT,
PRIMARY KEY (id),
FOREIGN KEY (student_id) REFERENCES student(id),
FOREIGN KEY (lesson_id) REFERENCES lesson(id)
);

INSERT INTO student VALUES
(null, 'akbar widyoseno', 'akbar', date('1996-04-30'), 'laki-laki'),
(null, 'widyoseno akbar', 'seno', date('1996-04-29'), 'laki-laki'),
(null, 'trissss', 'tris', date('1997-05-28'), 'perempuan'),
(null, 'ninaaa', 'nina', date('1998-07-11'), 'perempuan'),
(null, 'susiii', 'susi', date('1999-10-04'), 'perempuan')
;

INSERT INTO lesson VALUES
(null, 'lesson1', 'level1'),
(null, 'lesson2', 'level2'),
(null, 'lesson3', 'level3')
;

INSERT INTO score VALUES
(null, 1, 1, 90),
(null, 1, 2, 80),
(null, 1, 3, 70),
(null, 2, 1, 60),
(null, 2, 2, 70),
(null, 2, 3, 90),
(null, 3, 1, 80),
(null, 3, 2, 80),
(null, 3, 3, 70),
(null, 4, 1, 60),
(null, 4, 2, 70),
(null, 4, 3, 80),
(null, 5, 1, 90),
(null, 5, 2, 90),
(null, 5, 3, 60)
;