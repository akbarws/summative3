SELECT
st.name as fullname, st.BIRTHDATE as birthdate, st.gender, l.name as lessonName, l.level as lessonLevel, sc.score
FROM score as sc
JOIN student as st
ON sc.student_id = st.id
JOIN lesson as l
ON sc.lesson_id = l.id
ORDER BY fullname