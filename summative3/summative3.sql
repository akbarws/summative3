CREATE TABLE employee(
employee_id VARCHAR(100),
first_name VARCHAR(100),
last_name VARCHAR(100),
email VARCHAR(100),
phone_number VARCHAR(100),
hire_date VARCHAR(100),
job_id VARCHAR(100),
salary VARCHAR(100),
commision_pct VARCHAR(100),
manager_id VARCHAR(100),
department_id VARCHAR(100)
);

LOAD DATA INFILE 'summative2.txt' INTO TABLE employee
FIELDS
	TERMINATED BY '|'
	ESCAPED BY '\t'
LINES
	STARTING BY '|'
	TERMINATED BY '|'
;

UPDATE employee
SET
employee_id = TRIM(employee_id),
first_name = TRIM(first_name),
last_name = TRIM(last_name),
email = TRIM(email),
phone_number = TRIM(phone_number),
hire_date = TRIM(hire_date),
salary = TRIM(salary),
commision_pct = TRIM(commision_pct),
manager_id = TRIM(manager_id),
department_id = TRIM(department_id)
;

DELETE FROM employee
WHERE employee_id = 'EMPLOYEE_ID';

ALTER TABLE employee
MODIFY hire_date DATE;

ALTER TABLE employee
MODIFY salary DECIMAL(10,2);

ALTER TABLE employee
MODIFY commision_pct DECIMAL(10,2);