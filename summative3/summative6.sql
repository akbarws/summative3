CREATE TABLE departement (
department_id VARCHAR(50) NOT NULL,
department_name VARCHAR(50) NOT NULL,
manager_id VARCHAR(50) NOT NULL,
location_id VARCHAR(50) NOT NULL,
PRIMARY KEY (department_id)
);

LOAD DATA INFILE 'summative2-1.txt' INTO TABLE departement
FIELDS
	TERMINATED BY '|'
	ESCAPED BY '\t'
LINES
	STARTING BY '|'
	TERMINATED BY '|'
;

UPDATE departement
SET
department_id = TRIM(department_id),
department_name = TRIM(department_name),
manager_id = TRIM(manager_id),
location_id = TRIM(location_id)
;

DELETE FROM departement
WHERE department_id = 'DEPARTMENT_ID';