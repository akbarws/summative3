SELECT d.department_id, d.department_name, COUNT(e.first_name) as count
FROM departement as d
LEFT JOIN employee as e
ON d.department_id = e.department_id
GROUP BY d.department_id